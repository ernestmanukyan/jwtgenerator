﻿
using JWTGenerator;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace ConsoleApp1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Start();
            }
            catch (Exception)
            {
                Again();
            }
        }

        private static void Again()
        {
            try
            {
                Console.ForegroundColor = ConsoleColor.Red;

                Console.WriteLine("Start again ? --->yes / no");
                string result = Console.ReadLine().ToString();

                if (result == "yes")
                {
                    Start();
                }
                else if (result == "no")
                {
                    Environment.Exit(0);
                }
                else
                {
                    Again();
                }
            }
            catch 
            {
                Again();
            }
            
        }

        private static void Start()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Welcome to token generator !!!");
            Console.WriteLine("Is user PRO ? --> true/false");
            Console.ForegroundColor = ConsoleColor.Yellow;
            bool isPro = Convert.ToBoolean(Console.ReadLine().ToString());

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Set username");
            Console.ForegroundColor = ConsoleColor.Yellow;
            string userName = Console.ReadLine().ToString();

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Set userId");
            Console.ForegroundColor = ConsoleColor.Yellow;
            int userId = Convert.ToInt32(Console.ReadLine().ToString());


            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Set client id");
            Console.ForegroundColor = ConsoleColor.Yellow;
            int clientId = Convert.ToInt32(Console.ReadLine().ToString());

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Set DeviceId");
            Console.ForegroundColor = ConsoleColor.Yellow;
            int deviceId = Convert.ToInt32(Console.ReadLine().ToString());


            AppIdentifier data = new AppIdentifier() { AppVersion = "4.11.1", ClientId = clientId, DeviceId = deviceId, Generation = new Guid("c707ce41-a09b-4da4-bdc1-eeb79a616ced"), InstanceId = 11238232, IsPro = isPro, LocaleId = 1, Nickname = userName, UserId = userId }; ;

            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("*************************************Loading...*************************************");
            Console.WriteLine("*************************************RESULT*************************************");

            Console.WriteLine(GenerateToken(data, TimeSpan.FromHours(20)));



            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("If you want to generate a new token, type ----> start");
            Console.ForegroundColor = ConsoleColor.Yellow;
            string resultAnswer = Console.ReadLine().ToString();
            if (resultAnswer == "start")
            {
                Start();
            }
            else
            {
                Again();
            }

        }

        private static string GenerateToken(AppIdentifier instanceIdentifier, TimeSpan expireIn)
        {
            string issuer = "SoloLearn.Security.Bearer";
            string audience = "SoloLearn.Security.Bearer";
            string key = "E43DD58263BB52DFB65C6A7477F2C";

            string jsonTokenData = JsonConvert.SerializeObject(instanceIdentifier);

            var claims = new List<Claim>
                {
                    new Claim(JwtRegisteredClaimNames.Sid, jsonTokenData),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                };

            if (instanceIdentifier.UserId == 0)
            {
                claims.Add(new Claim(ClaimTypes.Role, "Public"));
            }
            else if (instanceIdentifier.UserId != 0)
            {
                claims.Add(new Claim(ClaimTypes.Role, "User"));
            }

            var token = new JwtSecurityToken
                (
                    issuer,
                    audience,
                    claims,
                    expires: DateTime.UtcNow.Add(expireIn),
                    notBefore: DateTime.UtcNow,
                    signingCredentials: new SigningCredentials(
                    new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key)),
                    SecurityAlgorithms.HmacSha256)
                );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
