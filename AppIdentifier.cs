﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JWTGenerator
{
    public class AppIdentifier
    {
        public int InstanceId { get; set; }
        public int UserId { get; set; }
        public string Nickname { get; set; }
        public int DeviceId { get; set; }
        public int ClientId { get; set; }
        public int LocaleId { get; set; }
        public string AppVersion { get; set; }
        public bool IsPro { get; set; }
        public Guid? Generation { get; set; }
    }
}
